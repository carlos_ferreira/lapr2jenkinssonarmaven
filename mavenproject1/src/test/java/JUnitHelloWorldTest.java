

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author cferreira
 */


import static java.lang.Boolean.TRUE;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.junit.Assert;
import static org.junit.Assert.assertFalse;


import pt.ipp.isep.dei.mavenproject1.JUnitHelloWorld;

public class JUnitHelloWorldTest {

	@BeforeClass
	public static void beforeClass() {
		System.out.println("Before Class");
	}

	@Before
	public void before() {
		System.out.println("Before Test Case");
	}

	@Test
	public void isGreaterTest() {
		System.out.println("Test");
		JUnitHelloWorld tester = new JUnitHelloWorld();
		assertTrue("Num 1 is greater than Num 2", tester.isGreater(4, 3));
               
	}

       @Test
	public void isLowerTest() {
	         System.out.println("Test");
		JUnitHelloWorld tester = new JUnitHelloWorld();
		assertTrue("Num 1 is Lower than Num 2", tester.isLower(2, 3));
                assertFalse("Num 1 is greater than Num 2", tester.isLower(6, 3));
                
	}
        
        @Test
	public void isEqualTest() {
		System.out.println("Test");
		JUnitHelloWorld tester = new JUnitHelloWorld();
		assertTrue("Num 1 is Equal than Num 2", tester.isEqual(2, 2));
                assertTrue("Num 1 is Equal than Num 2", tester.isEqual(1, 1));
                assertTrue("Num 1 is Equal than Num 2", tester.isEqual(0, 0));
                assertFalse("Num 1 is Equal than Num 2", tester.isEqual(1, 0));
	}
        
	@After
	public void after() {
		System.out.println("After Test Case");
	}

	@AfterClass
	public static void afterClass() {
		System.out.println("After Class");
	}
}